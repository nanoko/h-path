# H-Path Project

The H-Path project contains the LocationHashTracker h-ubu component which allows for the publication of an event on the topic `/path/change` each time the `location.hash` change. The event contains the location hash that has just change: 

```js
{ "path" : <hash> }
```
The H-Path does not require any library appart from h-ubu itself. If the browser does not support `window.onhashchange`, a degradation mode using `setInterval` take place. 


## Usage

Register the `LocationHashTracker` component on the hub.

```js
hub.registerComponent(new LocationHashTacker());
```

In order to get a notification each time the location change your component must subscribe to the `/path/change` event.

```js
// in start
this._hub.subscribe(this,"/path/change",this.mycallback);
```

## Example

The following example illustrate how to open an **altert** each time the `location.hash` change. 

```js
var comp = {
  _hub : null,
  getComponentName : function(){},
  mycallback : function(event){
    alert("Hash "+event.path);
  },
  start : function(){
    this._hub.subscribe(this,"/path/change",this.mycallback);
  },
  stop : function(){},
  configure : function(myHub,conf){
    this._hub = myHub;
  }
};


hub.registerComponent(new LocationHashTacker());
hub.registerComponent(comp);
hub.start();
```
