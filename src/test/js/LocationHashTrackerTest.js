const TESTMAX = 10; //Maximum number of tests that run in a loop.

describe("LocationHashTracker component test suite", function () {
    var lhtComp = null; //LocationHashTracker instance

    beforeEach(function(){
        location.hash = null;
        lhtComp = new LocationHashTacker();
    });

    afterEach(function () {
        hub.reset();
        lhtComp = null;
    });


    it("Should start properly", function() {
        //Given (lhtComp)

        //When
        hub.registerComponent(lhtComp);
        hub.start();
     });

    it("Should have topic `/path/change` by default", function() {
        //Given (lhtComp)

        //Then
        expect(lhtComp.getTopic()).toBe("/path/change");
    });

    it("Should change topic when a valid topic name is passed in the configuration", function() {
        //Given (lhtComp)
        var topic = "/toto";

        //When
        hub.registerComponent(lhtComp,{"topic" :topic});
        hub.start();

        //Then
        expect(lhtComp.getTopic()).toBe(topic);
    });

    it("Should change topic when a valid topic name is passed in the configuration 2", function() {
        //Given (lhtComp)
        var topic = "/tosa1d/saASf121fto/sdf74";

        //When
        hub.registerComponent(lhtComp,{"topic" :topic});
        hub.start();

        //Then
        expect(lhtComp.getTopic()).toBe(topic);
    });

    it("Should throw Exception when topic name in configuration is not valid", function() {
        //Given (lhtComp)
        var topic = "toto";

        //When
        var register = function(){ hub.registerComponent(lhtComp,{"topic" :topic});};

        //Then
        expect(register).toThrow();
    });

    it("Should throw Exception when topic name in configuration is not valid 2", function() {
        //Given (lhtComp)
        var topic = "/toto{";

        //When
        var register = function(){ hub.registerComponent(lhtComp,{"topic" :topic});};

        //Then
        expect(register).toThrow();
    });

    it("Should publish location.hash after starting", function() {
        //Given (lhtComp)
        location.hash = "#root";
        spyOn(HUBU.Eventing.prototype,'publish');

        //When
        hub.registerComponent(lhtComp);
        hub.start();

        //Then
        expect(HUBU.Eventing.prototype.publish).toHaveBeenCalledWith(lhtComp, "/path/change",{"path" : "#root"});
    });

    it("Should publish location.hash when onhashchange is called[with window.onhashchange defined].", function() {
        //Given (lhtComp)
        location.hash = "#root";
        window.onhashchange = null;
        spyOn(HUBU.Eventing.prototype,'publish');
        hub.registerComponent(lhtComp);
        hub.start();

        //When
        location.hash = "#home";
        window.onhashchange();

        //Then
        expect(HUBU.Eventing.prototype.publish).toHaveBeenCalledWith(lhtComp, "/path/change",{"path" : "#home"});
    });

    it("Should publish location.hash each time onhashchange is called[with window.onhashchange defined].", function() {
        //Given (lhtComp)
        location.hash = "#root";
        window.onhashchange = null;
        spyOn(HUBU.Eventing.prototype,'publish');
        hub.registerComponent(lhtComp);
        hub.start();

        for (var i = TESTMAX - 1; i >= 0; i--) {
            //WHEN
            location.hash = "#"+i;
            window.onhashchange();

            //Then
            expect(HUBU.Eventing.prototype.publish).toHaveBeenCalledWith(lhtComp, "/path/change",{"path" : "#"+i});
        }
    });

    it("Should publish location.hash when it change[degradation - setInterval mode].", function() {
        //Given (lhtComp)
        document.documentMode = 1; //force degradation
        spyOn(HUBU.Eventing.prototype,'publish');
        jasmine.Clock.useMock();

        hub.registerComponent(lhtComp);
        hub.start();

        //When
        location.hash = "#home";
        jasmine.Clock.tick(51); //the callback in lhtComp should have been call.


        //Then
        expect(HUBU.Eventing.prototype.publish).toHaveBeenCalledWith(lhtComp, "/path/change",{"path" : "#home"});
    });
});

